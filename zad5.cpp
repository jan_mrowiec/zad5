/*
Program po uruchomieniu losuje przykladowe komunikaty, a pozniej zaleznie od poziomu wypisuje je na ekran. Program wpisuje WSZYSTKIE komunikaty do pliku log_all.txt.
Zaleznie od uzytkownika program moze wpisywac komunikaty widoczne tylko dla uzytkownika w pliku user.txt.
Program korzysta z bibliotek windowsowych.


bitbucket.org/jan_mrowiec/zad5/src/master/zad5.cpp
*/
#include <windows.h>
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <string>
#include <conio.h>

using namespace std;

enum Level
{
    SEVERE = 6,
    WARNING = 5,
    INFO = 4,
    CONFIG = 3,
    FINE = 2,
    FINER = 1,
    FINEST = 0,
};

class Dziennik //klasa glowna
{
    Level l;
    FILE* f_user;//adres pliku logow uzytkownika
    FILE* f_all;//adres pliku przechowujace wszystkie logi
    bool wToFile;//czy zapisywac dane do pliku uzytkownika

public:
    Level getLevel()//funkcja zwraca poziom
    {
        return l;
    }
    void cout_Level()//funkcja wypisuje poziom uzytkownika
    {
        string s;
        switch(l)
        {
        case SEVERE:
            s = "\nPoziom: SEVERE\n";
            break;
        case WARNING:
            s = "\nPoziom: WARNING\n";
            break;
        case INFO:
            s = "\nPoziom: INFO\n";
            break;
        case CONFIG:
            s = "\nPoziom: CONFIG\n";
            break;
        case FINE:
            s = "\nPoziom: FINE\n";
            break;
        case FINER:
            s = "\nPoziom: FINER\n";
            break;
        case FINEST:
            s = "\nPoziom: FINEST\n";
            break;
        }
        fprintf(f_all, &s[0]);
        cout << s;
        if(wToFile)
            fprintf(f_user, &s[0]);
    }
    void setLevel(Level l)//funkcja zmienia poziom uzytkownika
    {
        this->l = l;
    }
    void setwToFile(bool f)//zmienie statusu zapisu do pliku
    {
        if(!(f ^ wToFile))
            return;
        if(f)
        {
            f_user = fopen("user.txt", "a");
            fprintf(f_user, "Poczatek sesji \n");
        }
        else
        {
            fprintf(f_user, "Koniec sesji \n");
            fclose(f_user);
        }
    }
    Dziennik(Level l, bool f)//konstrukotr
    {
        this->l = l;

        f_all = fopen("log_all.txt", "a");//stworzenie pliku z wszystkimi logami (w trybie dopisywania)
        fprintf(f_all, "Poczatek sesji \n");
        wToFile = f;
        if(f){ //jezeli wToFile przwadziwe stworz plik uzytkownika
            f_user = fopen("user.txt", "a");
            fprintf(f_user, "Poczatek sesji \n");
        }
    }
    ~Dziennik()//dekonstrukotr
    {
        setwToFile(0);//zamkniecie pliku uzytkownika jesli istenije
        fprintf(f_user, "Koniec sesji sesji \n");//zamkniecie log_all
        fclose(f_all);
    }
    void log(string s, Level nl)//funkcja wypisujaca log
    {
        switch(nl){ //dopisanie poziomu do stringa wynikowego
        case SEVERE:
            s = "SEVERE: " + s;
            break;
        case WARNING:
            s = "WARNING: " + s;
            break;
        case INFO:
            s = "INFO: " + s;
            break;
        case CONFIG:
            s = "CONFIG: " + s;
            break;
        case FINE:
            s = "FINE: " + s;
            break;
        case FINER:
            s = "FINER: " + s;
            break;
        case FINEST:
            s = "FINEST: " + s;
            break;
        }
        s += '\n';
        fprintf(f_all, &s[0]);//wpisz komunikat do pliku log_all
        if(nl >= l) //jezeli poziom komunkiaty wyszszy lub rowny poziomowi dziannika - wypisz go
        {
            cout << s;
            if(wToFile)//jezeli wToFile prawdziwe wpisz log do pliku uzytkownika
                fprintf(f_user, &s[0]);
        }

    }
};


Level pick_Level()
{
    cout << "Wybierz poziom logowania" << endl;
    cout << "[6]SEVERE" << endl;
    cout << "[5]WARNING" << endl;
    cout << "[4]INFO" << endl;
    cout << "[3]CONFIG" << endl;
    cout << "[2]FINE" << endl;
    cout << "[1]FINER" << endl;
    cout << "[0]FINEST" << endl;
    int l;
    cin >> l;
    return (Level)l;
}

void clear_buf()//czyszczenie bufora wejscia
{
    while(kbhit())
        getch();
}

int main()
{
    bool f;
    cout << "Zapis do pliku? [t/n]" << endl;
    char c;
    cin >> c;
    if(c == 't' || c == 'T')f = 1;
    else f = 0;
    Dziennik d(pick_Level(), f); //stworzenie zmiennej d
    int i = 0; //zmienna liczaca numer komunikatu
    while(1)
    {
        d.cout_Level(); //wypisanie poziomu
        cout << "Nacisnij dowolny przycisk by przerwac" << endl << endl;
        while(1)//losowanie kolejnych komunikatow az do nacisniecia przycisku
        {
            int r = rand() % 7;
            string out = "Komunikat " + to_string(i); //stworzenie komunikatu
            d.log(out, (Level)r);//wypisanie komunikatu
            i++;
            Sleep(1000);
            if(kbhit())//sprawdzenie warunku wyjscia z petli
                break;
        }
        clear_buf();//wyczesczenie bufora
        cout << "Zmienic poziom logowania? [t/n]" << endl;
        cin >> c;
        if(c == 't' || c == 'T') d.setLevel(pick_Level());
        cout << "Wyjsc z programu? [t/n]" << endl;
        cin >> c;
        if(c == 't' || c == 'T') break;//wyjscie z programu
    }
    return 0;
}
